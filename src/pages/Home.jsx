import React from 'react';
import { Button, Container } from 'react-bootstrap';
import Highlights from '../components/Highlights';

const Home = () => {

  return (
    <Container>
        
        <div className='text-center m-5 p-5'>
            <h1>"Welcome to Shanstyle"<br /><br/>
            <h5><i>"We provide quality beauty and skincare products!"</i></h5>
            <br/> 
            <Button class='bg-primary' type="submit" id="submitBtn" href='/products'>
                                Browse Products
            </Button>
            </h1>
        </div>
        <Highlights />     
    </Container>
  )     
};

export default Home;
