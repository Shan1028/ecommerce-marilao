import React from 'react';
import { Card, Container } from 'react-bootstrap';


const Footer = () => {
  return (

    <Card.Footer className='mt-5'>
        <div >
            <Container style={{ width: 'auto' }}>
                <Card.Body className='text-center'>
                    <Card.Title>ShanStyle || &copy; All Rights Reserved || 2022</Card.Title>

                    <a href="https://facebook.com" target='_blank'>
                    <img className='p-2' src="https://img.icons8.com/ios-filled/32/000000/facebook--v2.png" alt='facebook icon'/>
                    </a>
                    <a href="https://twitter.com" target='_blank'>
                    <img className='p-2' src="https://img.icons8.com/ios-glyphs/32/000000/twitter--v2.png" alt='twitter icon'/>
                    </a>
                    <a href="https://instagram.com" target='_blank'>
                    <img className='p-2' src="https://img.icons8.com/ios-filled/32/000000/instagram-new--v2.png" alt='Instagram icon'/>
                    </a>    
                 </Card.Body>
            </Container>
        </div>
    </Card.Footer>  
  );
};

export default Footer;
