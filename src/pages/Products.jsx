import React from 'react';
import ProductLists from '../ProductLists'
import Itemcard from '../components/Itemcard'

const Products = () => {


  return (
    <div className='mx-auto'>
        <h1 className='text-center mt-3'>All Products</h1>
            <section className='py-4 container'>
                <div className='row justify-content-center'>
                   {ProductLists.productData.map((item, index)=>{
                        return(
                            <Itemcard 
                                img={item.img} 
                                title={item.title} 
                                desc={item.desc} 
                                price={item.price}
                                item={item} 
                                key={index}
                            />
                        )
                    })}
                    
                </div>
            </section>
        </div>
  )
  
};

export default Products;
