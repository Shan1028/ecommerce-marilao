import React, { useEffect, useState } from 'react';
import UserContext, { UserProvider } from './UserContext';
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import AppNavbar from './components/AppNavbar'
import Home from './pages/Home';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Register from './pages/Register';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import Footer from './pages/Footer';
import Products from './pages/Products';
import ProductView from './pages/ProductView'
import Error from './pages/Error';
import { CartProvider } from 'react-use-cart';
import Cart from './pages/Cart'


const App = () => {

  const [ user, setUser ] = useState({
    //email: localStorage.getItem('email')
    id: null,
    isAdmin: null
  })

  // Function for clearing localStorage on logout
  const unsetUser = () => {
    localStorage.clear();

    setUser({
			id: null,
			isAdmin: null
		});

  }

  //Because our user state's values are reset to null every time the user reloads the page (thus logging the user out), we want to use React's useEffect hook to fetch the logged-in user's details when the page is reloaded. By using the token saved in localStorage when a user logs in, we can fetch the their data from the database, and re-set the user state values back to the user's details.
  useEffect(() => {
    console.log(user);
    console.log(localStorage);

    fetch(`http://localhost:3000/api/users/details`, {
			headers: {
				Authorization: `Bearer ${ localStorage.getItem('token') }`
			}
		})
		.then(res => res.json())
		.then(data => {

			// Set the user states values with the user details upon successful login.
			if (typeof data._id !== "undefined") {

				setUser({
					id: data._id,
					isAdmin: data.isAdmin
				});

			// Else set the user states to the initial values
			} else {

				setUser({
					id: null,
					isAdmin: null
				});

			}

		})


  },[])

  return (
      <UserProvider value={{user, setUser, unsetUser}}>
        <CartProvider>
        <Router>
        <AppNavbar />
        <Switch>
          <Route exact path='/'>
            <Home />
          </Route>
          <Route exact path='/products'>
            <Products />
          </Route>
          <Route exact path='/products/:productId'>
            <ProductView />
          </Route>
          <Route exact path='/cart'>
            <Cart />
          </Route>
          <Route exact path='/login'>
            <Login />
          </Route>
          <Route exact path='/logout'>
            <Logout />
          </Route>
          <Route exact path='/register'>
            <Register />
          </Route>
          <Route exact path='/cart'>
            <Cart />
          </Route>
          <Route >
            <Error />
          </Route>
        </Switch>
        <Footer />
      </Router>
      </CartProvider>
      </UserProvider>
  )
};

export default App;
