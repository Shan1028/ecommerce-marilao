import React from 'react';
import { Button, Card, CardGroup } from 'react-bootstrap';


const Highlights = () => {
  return (
    
    <div>
        <h1 className='text-center mt-5'>Featured Products</h1>
        <CardGroup className='mt-5 mx-auto'>
        <Card className='m-3 p-4' style={{ borderRadius: '30px'}} >
            <Card.Img style = {{ width: '300px', height: '300px'}} variant="top" src="../images/retinolcream.jpg" />
            <Card.Body>
            <Card.Title>Retinol Collagen Cream</Card.Title>
            <Card.Text>
                This is a wider card with supporting text below as a natural lead-in to
                additional content. This content is a little bit longer.
            </Card.Text>
            </Card.Body>
            <Button variant='primary' type='submit' href='/products'>Go to Products</Button>
            
        </Card>
        <Card className='m-3 p-4' style={{ borderRadius: '30px'}} >
            <Card.Img style = {{ width: '300px', height: '300px'}} variant="top" src="../images/neutrogenaspf.jpg" />
            <Card.Body>
            <Card.Title>Neutrogena with SPF</Card.Title>
            <Card.Text>
                This card has supporting text below as a natural lead-in to additional
                content.{' '}
            </Card.Text>
            </Card.Body>
            <Button variant='primary' type='submit' href='/products'>Go to Products</Button>
        </Card>
        <Card className='m-3 p-4' style={{ borderRadius: '30px'}} >
            <Card.Img 
            style = {{ 
                width: '300px',
                height: '300px',
                flexWrap: 'wrap'}} variant="top" src="../images/truskinvitaminc.jpg" />
            <Card.Body>
            <Card.Title>Truskin Vitamin C</Card.Title>
            <Card.Text>
                This is a wider card with supporting text below as a natural lead-in to
                additional content. This card has even longer content than the first to
                show that equal height action.
            </Card.Text>
            </Card.Body>
            <Button variant='primary' type='submit' href='/products'>Go to Products</Button>
        </Card>
    </CardGroup>
    </div>
    
    
    );
};

export default Highlights;
