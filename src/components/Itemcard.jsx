import React from 'react'
import { useCart } from 'react-use-cart'

const Itemcard = (props) => {

    const { addItem } = useCart();
    return (
        <div className='col-12 col-md-4  mx-auto mb-4'>
            <div class="card p-0 overflow-hidden h-100 shadow" style={{borderRadius: '40px'}}>
                <img src={props.img} class="card-img-top img-fluid p-3"
                style={{ width: '400px', height: '320px'}} />
                <div class="card-body text-center">
                <h5 class="card-title">{props.title}</h5>
                <h5 class="card-price">PhP {props.price}</h5>
                <p class="card-text"><b>Description:</b><br />{props.desc}</p>
                <button class="btn btn-success" onClick={()=>addItem(props.item)}>Add to Cart</button>
                </div>
            </div>
            
        </div>
        
    )
}

export default Itemcard
