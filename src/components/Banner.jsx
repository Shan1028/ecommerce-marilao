import { Row, Col } from 'react-bootstrap';
import { Link } from 'react-router-dom'
import React from 'react';

export default function Banner({ bannerProps }) {

    const { title, content, destination, label } = bannerProps;

    return (
        <Row>
            <Col className='text-center mt-5'>

                <h1>{title}</h1>
                <p>{content}</p>
                <Link className="btn btn-danger" to={destination}>{label}</Link>

            </Col>
        </Row>
    )
}